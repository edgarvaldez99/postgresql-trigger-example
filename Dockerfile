FROM postgres:9.4

RUN apt-get update && apt-get install -y postgresql-plpython-9.4
  # && ln -s /usr/lib/postgresql/plpython3.so /usr/local/lib/postgresql/plpython3.so \
  # && ln -s /usr/share/postgresql/extension/plpython3u.control /usr/local/share/postgresql/extension/plpython3u.control \
  # && ln -s /usr/share/postgresql/extension/plpython3u--1.0.sql /usr/local/share/postgresql/extension/plpython3u--1.0.sql \
  # && ln -s /usr/share/postgresql/extension/plpython3u--unpackaged--1.0.sql /usr/local/share/postgresql/extension/plpython3u--unpackaged--1.0.sql

COPY example/dump-postgres.backup /backups/

COPY restore.sh install-plpythonu.sh /docker-entrypoint-initdb.d/
