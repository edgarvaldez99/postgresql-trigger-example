-- CREATE PROCEDURE FOR TRIGGER
CREATE OR REPLACE FUNCTION plpython_procedure_proyectos_participacion_ciudadana_biudt() RETURNS trigger AS $$

import json
import urllib2

URL = 'http://51.15.15.75:13000/project/save-by-trigger'

dictdata = { "data": { "new": TD["new"], "old": TD["old"] }, "operation": TD["event"] }
jsondata = json.dumps(dictdata)
clen = len(jsondata)
request = urllib2.Request(URL, jsondata, { "Content-Type": "application/json", "Content-Length": clen })
response = urllib2.urlopen(request)
response.read()

$$ LANGUAGE plpythonu;

-- CREATE TRIGGER
CREATE TRIGGER trigger_proyectos_participacion_ciudadana_biudt BEFORE INSERT OR UPDATE OR DELETE ON proyectos_participacion_ciudadana
    FOR EACH ROW EXECUTE PROCEDURE plpython_procedure_proyectos_participacion_ciudadana_biudt();
