CREATE OR REPLACE FUNCTION procedure_for_save_proyectos_participacion_ciudadana()
  RETURNS trigger AS
$BODY$
DECLARE
  record_id integer;
BEGIN

  RAISE NOTICE 'TRIGGER ON %', TG_TABLE_NAME;
  RAISE NOTICE 'TG_OP = %', TG_OP;

  IF TG_OP = 'DELETE' THEN
    RAISE NOTICE 'WITH OLD.id = %', OLD.id;
    DELETE FROM proyectos_participacion_ciudadana WHERE id = OLD.id;
    RETURN NEW;
  END IF;

  IF TG_TABLE_NAME = 'proyectos' THEN
    record_id := NEW.id;
  ELSIF TG_TABLE_NAME = 'proyectos_ficha_tecnica' THEN
    record_id := NEW.proyecto_id;
  END IF;

  RAISE NOTICE 'WITH record_id = %', record_id;

  CASE TG_OP
    WHEN 'INSERT' THEN
      INSERT INTO proyectos_participacion_ciudadana
      SELECT p.id,
        p.nombre AS name,
        p.descripcion,
        'project_triggered_'::text || p.id AS code,
        pf.ente_ejecutor,
        pf.ubicacion,
        pf.superficie,
        pf.fuente_financiacion
      FROM proyectos p
        LEFT JOIN proyectos_ficha_tecnica pf
        ON p.id = pf.proyecto_id
      WHERE p.id = record_id;

    WHEN 'UPDATE' THEN
      IF TG_TABLE_NAME = 'proyectos' THEN
        UPDATE proyectos_participacion_ciudadana
        SET
          "name"=NEW.nombre,
          descripcion=NEW.descripcion,
          code='project_triggered_'::text || id
        WHERE id = record_id;

      ELSIF TG_TABLE_NAME = 'proyectos_ficha_tecnica' THEN
        UPDATE proyectos_participacion_ciudadana
        SET
          ente_ejecutor=NEW.ente_ejecutor,
          ubicacion=NEW.ubicacion,
          superficie=NEW.superficie,
          fuente_financiacion=NEW.fuente_financiacion
        WHERE id = record_id;

      END IF;
  END CASE;

  RETURN NEW;
END;
$BODY$ LANGUAGE plpgsql;


-- CREATE TRIGGER
CREATE TRIGGER trigger_proyectos_budt BEFORE UPDATE OR DELETE ON proyectos
    FOR EACH ROW EXECUTE PROCEDURE procedure_for_save_proyectos_participacion_ciudadana();

-- CREATE TRIGGER
CREATE TRIGGER trigger_proyectos_ait AFTER INSERT ON proyectos
    FOR EACH ROW EXECUTE PROCEDURE procedure_for_save_proyectos_participacion_ciudadana();


-- CREATE TRIGGER
CREATE TRIGGER trigger_proyectos_ficha_tecnica_budt BEFORE UPDATE OR DELETE ON proyectos_ficha_tecnica
    FOR EACH ROW EXECUTE PROCEDURE procedure_for_save_proyectos_participacion_ciudadana();

-- CREATE TRIGGER
CREATE TRIGGER trigger_proyectos_ficha_tecnica_ait AFTER INSERT ON proyectos_ficha_tecnica
    FOR EACH ROW EXECUTE PROCEDURE procedure_for_save_proyectos_participacion_ciudadana();
