CREATE TABLE public.proyectos (
  id serial NOT NULL,
  nombre varchar NOT NULL,
  descripcion varchar NULL,
  CONSTRAINT proyectos_pk PRIMARY KEY (id)
);


CREATE TABLE public.proyectos_ficha_tecnica (
  id serial NOT NULL,
  proyecto_id int NOT NULL,
  ente_ejecutor varchar NOT NULL DEFAULT 'Ministerio de Obras Públicas y Comunicaciones (MOPC)',
  ubicacion varchar NULL,
  superficie varchar NULL,
  fuente_financiacion varchar NULL,
  CONSTRAINT proyectos_ficha_tecnica_pk PRIMARY KEY (id),
  CONSTRAINT proyectos_ficha_tecnica_uk UNIQUE (proyecto_id),
  CONSTRAINT proyectos_ficha_tecnica_fk FOREIGN KEY (proyecto_id) REFERENCES public.proyectos(id) ON DELETE CASCADE
);


CREATE TABLE public.proyectos_participacion_ciudadana AS
  SELECT p.id,
    p.nombre AS name,
    p.descripcion AS descripcion,
    'project_triggered_'::text || p.id AS code,
    pf.ente_ejecutor AS ente_ejecutor,
    pf.ubicacion AS ubicacion,
    pf.superficie AS superficie,
    pf.fuente_financiacion AS fuente_financiacion
  FROM proyectos p
    LEFT JOIN proyectos_ficha_tecnica pf
    ON p.id = pf.proyecto_id;
