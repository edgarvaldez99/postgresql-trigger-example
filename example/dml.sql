-- INSERT
INSERT INTO public.proyectos
(descripcion, nombre)
VALUES('Proyecto nro 1', 'proyecto 1');

-- UPDATE
UPDATE public.proyectos
SET nombre='proyecto 2'
where id=1;

UPDATE public.proyectos
SET descripcion='Proyecto número 2'
where id=1;

-- DELETE
DELETE FROM public.proyectos WHERE id=1;


-- INSERT TO ficha_tecnica
INSERT INTO public.proyectos_ficha_tecnica
(proyecto_id, ubicacion, superficie, fuente_financiacion)
VALUES(1, 'Ubicación', 'Superficie', 'Fuente');


-- INSERT TO proyectos_participacion_ciudadana
INSERT INTO proyectos_participacion_ciudadana
      SELECT p.id,
        p.nombre AS name,
        p.descripcion,
        'project_triggered_'::text || p.id AS code,
        pf.ente_ejecutor,
        pf.ubicacion,
        pf.superficie,
        pf.fuente_financiacion
      FROM proyectos p
        LEFT JOIN proyectos_ficha_tecnica pf
        ON p.id = pf.proyecto_id
      WHERE p.id = 1;
